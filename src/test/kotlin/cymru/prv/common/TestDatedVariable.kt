package cymru.prv.common

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.time.LocalDate

/**
 * @author Preben Vangberg
 * @since 1.0.0
 */
class TestDatedVariable {

    @Test
    fun `should be able to fetch last value`(){
        val variable = DatedVariable("")
        variable.setValue(LocalDate.now(), "Hello")
        assertEquals("Hello", variable.getValue())
    }

    @Test
    fun `should be able set value in the future`(){
        val variable = DatedVariable("")
        variable.setValue(LocalDate.now().plusDays(10), "Hello")
        assertEquals("", variable.getValue())
        assertEquals("Hello", variable.getValue(LocalDate.now().plusDays(20)))
    }

    @Test
    fun `should be able to add two values and fetch them based on date`(){
        val variable = DatedVariable("")
        variable.setValue(LocalDate.now().minusDays(10), "Hello")
        variable.setValue(LocalDate.now().minusDays(5), "World")
        assertEquals("", variable.getValue(LocalDate.now().minusDays(12)))
        assertEquals("Hello", variable.getValue(LocalDate.now().minusDays(8)))
        assertEquals("World", variable.getValue())
    }

    @Nested
    inner class TestPruning {

        @Test
        fun `Adding the same value on two dates should not increase size if pruning is true`(){
            val variable = DatedVariable(false)
            assertEquals(0, variable.size())
            variable.setValue(LocalDate.now().minusDays(10), true)
            assertEquals(1, variable.size())
            variable.setValue(LocalDate.now().minusDays(5), true, prune=true)
            assertEquals(1, variable.size())
        }

    }

    @Nested
    inner class TestAfter {

        @Test
        fun `should only return values during period`(){
            val test = DatedVariable(false)
            test.setValue(LocalDate.of(1980, 1, 1), true)
            test.setValue(LocalDate.of(1990, 1, 1), false)
            test.setValue(LocalDate.of(2019, 6, 3), true)
            test.setValue(LocalDate.of(2019, 8, 5), false)

            val values = test.getValues(
                after = LocalDate.of(2019, 6, 3),
                date = LocalDate.of(2019, 8, 4))
            assertEquals(1, values.size)
        }

        @Test
        fun `should return previous change if included in date span`(){
            val test = DatedVariable(false)
            test.setValue(LocalDate.of(1980, 1, 1), true)
            test.setValue(LocalDate.of(1990, 1, 1), false)
            test.setValue(LocalDate.of(2019, 6, 3), true)
            test.setValue(LocalDate.of(2019, 8, 5), false)

            val values = test.getValues(
                after = LocalDate.of(2019, 6, 1),
                date = LocalDate.of(2019, 8, 4))
            assertEquals(2, values.size)
            assertFalse(values[0].value)
            assertTrue(values[1].value)
        }

        @Test
        fun `should included last change if multiple`(){
            val test = DatedVariable(false)
            test.setValue(LocalDate.of(1980, 1, 1), true)
            test.setValue(LocalDate.of(1990, 1, 1), false)
            test.setValue(LocalDate.of(2019, 6, 3), true)
            test.setValue(LocalDate.of(2019, 8, 5), false)

            val values = test.getValues(
                after = LocalDate.of(2019, 6, 1),
                date = LocalDate.of(2019, 8, 7))
            assertEquals(3, values.size)
            assertFalse(values[0].value)
            assertTrue(values[1].value)
            assertFalse(values[2].value)
        }

    }

}