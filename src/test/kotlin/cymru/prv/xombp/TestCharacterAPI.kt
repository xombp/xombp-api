package cymru.prv.betty

import cymru.prv.xombp.*
import org.json.JSONObject
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.time.LocalDate

/**
 * A simple test class to test the character
 * end-point
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestCharacterAPI {

    @Nested
    inner class BasicInformation {

        @Test
        fun `should return the character's name`(){
            val name = "Hywel ap Iorwerth"
            val xombp = XOMBP()
            val char = Character(xombp, name, User(xombp,"Prv"))
            val obj = char.toJson()
            assertEquals(name, obj.getString("name"))
        }

        @Test
        fun `should return the character's fullname`(){
            val name = "Hywel ap Iorwerth"
            val xombp = XOMBP()
            val char = Character(xombp, name, User(xombp,"Prv"))
            char.honours[Honours.KOBC] = Honour(LocalDate.now(), "Sir")
            val obj = char.toJson()
            assertEquals(name, obj.getString("name"))
            assertEquals("Sir $name KOBC", obj.getString("fullname"))
        }

    }

    @Nested
    inner class PositionInformation {

        @Test
        fun `should return positions`(){
            val xombp = XOMBP()
            val prv = Character(xombp, "Hywel ap Iorwerth", User(xombp,"Prv"))
            val zippy = Character(xombp, "Zach Henderson", User(xombp,"Zippy"))
            val caprice = Character(xombp, "Nicholas Clack", User(xombp,"Caprice"))
            Position(xombp, "speaker", JSONObject()
                .put("name", "Speaker")
                .put("type", "other")
                .put("active", JSONObject().put("2000-01-01", true))
                .put("holder", JSONObject()
                    .put("2020-07-01", "Nicholas Clack")
                    .put("2021-01-05", "Zach Henderson")
                    .put("2021-07-06", "Hywel ap Iorwerth"))
            )

            fun validatePositions(
                character: JSONObject,
                from: LocalDate,
                to: LocalDate? = null,
                precededBy: String? = null,
                succeededBy: String? = null
            ){
                val obj = character.getJSONArray("positions").getJSONObject(0)
                assertEquals(from, obj.get("from"))
                when(to){
                    null -> assertFalse(obj.has("to"))
                    else -> assertEquals(to, obj.get("to"))
                }
                when(precededBy){
                    null -> assertFalse(obj.has("precededBy"))
                    else -> assertEquals(precededBy, obj.getString("precededBy"))
                }
                when(succeededBy){
                    null -> assertFalse(obj.has("succeededBy"))
                    else -> assertEquals(succeededBy, obj.getString("succeededBy"))
                }
            }

            validatePositions(
                caprice.toJson(),
                LocalDate.parse("2020-07-01"),
                LocalDate.parse("2021-01-04"),
                succeededBy = "Zach Henderson"
            )

            validatePositions(
                zippy.toJson(),
                LocalDate.parse("2021-01-05"),
                LocalDate.parse("2021-07-05"),
                precededBy = "Nicholas Clack",
                succeededBy = "Hywel ap Iorwerth"
            )

            validatePositions(
                prv.toJson(),
                LocalDate.parse("2021-07-06"),
                precededBy = "Zach Henderson"
            )
        }

    }


}