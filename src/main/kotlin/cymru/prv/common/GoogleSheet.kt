package cymru.prv.common

import java.net.URL

data class GoogleSheet(val file: String, val gid: String){
    fun toURL() = URL("https://docs.google.com/spreadsheets/d/$file/export?gid=$gid&format=tsv")
}