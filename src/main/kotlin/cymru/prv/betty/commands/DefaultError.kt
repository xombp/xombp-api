package cymru.prv.betty.commands

import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import java.awt.Color


fun createError(msg: String, title: String = "An error occurred"): MessageEmbed {
    val builder = EmbedBuilder()
    builder.setTitle(title)
    builder.setColor(Color.RED)
    builder.setDescription(msg)
    return builder.build()
}