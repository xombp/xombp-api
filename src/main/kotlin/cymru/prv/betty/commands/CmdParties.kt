package cymru.prv.betty.commands

import cymru.prv.betty.Betty
import net.dv8tion.jda.api.EmbedBuilder

object CmdParties: Command(
    "Prints a list of parties",
    "",
    {
        val builder = EmbedBuilder()

        builder.setTitle("List of Parties")
        builder.setColor(Betty.defaultColor)

        builder.addField(
            "Currently Active",
            Betty.XOMBP.parties.values
                .distinct()
                .filter { it.active.getValue() && it.active.size() > 0 }
                .sortedBy { it.name.getValue() }
                .joinToString(separator = "\n")
                { "${it.emote.getValue()} ${it.name.getValue()}".trim() },
            false
        )

        builder.addField(
            "Permanently Active",
            Betty.XOMBP.parties.values
                .distinct()
                .filter { it.active.getValue() && it.active.size() == 0 }
                .sortedBy { it.name.getValue() }
                .joinToString(separator = "\n")
                { "${it.emote.getValue()} ${it.name.getValue()}".trim() },
            false
        )

        var part = 1
        val sb = StringBuilder()
        Betty.XOMBP.parties.values
            .distinct()
            .filter { !it.active.getValue() }
            .sortedBy { it.name.getValue() }
            .forEach {
                sb.append("${it.emote.getValue()} ${it.name.getValue()}".trim())
                    .append("\n")
                if(sb.length > 900){
                    builder.addField(
                        "Defunct Parties: Part $part",
                        sb.toString(),
                        false
                    )
                    part++
                    sb.clear()
                }
            }

        if(sb.isNotEmpty()){
            builder.addField(
                "Defunct Parties: Part $part",
                sb.toString(),
                false
            )
        }


        builder.build()
    }
)

