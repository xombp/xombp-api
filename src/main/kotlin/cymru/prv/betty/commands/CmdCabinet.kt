package cymru.prv.betty.commands

import cymru.prv.betty.Betty
import cymru.prv.xombp.PositionType
import net.dv8tion.jda.api.EmbedBuilder

object CmdCabinet: Command(
    "Prints the current cabinet",
        "[date]",
    { params ->

        val date = fetchDate(params)

        val builder = EmbedBuilder()
        builder.setTitle("Her Majesty's Government")

        val ministry = Betty.XOMBP.ministries.getValue(date)
        if(ministry != null){
            builder.addField("Government", ministry.numberAsNumeral(), true)
            builder.addField("Ministry", ministry.name, true)
            if(ministry.nickname != null)
                builder.addField("Nickname", ministry.nickname, true)
        }

        val pm = Betty.XOMBP.positions["pm"]?.holder?.getValue(date)

        if(pm != null){
            builder.addField(
                "Prime Minister",
                pm.getFullName(date),
                false
            )
            builder.setThumbnail(
                pm.facesteal.link
            )
            builder.setColor(pm.affiliation.getValue(date).color.getValue())
        }

        Betty.XOMBP.positions.values
            .distinct()
            .filter { it.active.getValue(date ) }
            .filter { it.id != "pm" }
            .filter { it.type == PositionType.GOVERNMENT }
            .forEach {
                val holder = it.holder.getValue(date)
                val name = holder?.name?.getValue(date) ?: "Vacant"
                val affil = holder?.affiliation?.getValue(date) ?: Betty.XOMBP.parties.getValue("indy")
                builder.addField(
                    it.name.getValue(date),
                    "%s %s".format(
                        affil.emote.getValue(date),
                        name
                    ),
                    false
                )
            }

        builder.build()
    }
)