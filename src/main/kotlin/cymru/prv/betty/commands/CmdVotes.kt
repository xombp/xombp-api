package cymru.prv.betty.commands

import cymru.prv.betty.Betty
import cymru.prv.common.GoogleSheet
import cymru.prv.xombp.House
import cymru.prv.xombp.legislation.Vote
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.EmbedType
import net.dv8tion.jda.api.entities.MessageEmbed
import org.http4k.routing.path
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

/**
 * @author Preben Vangberg
 */
object CmdVotes: Command(
    "Lists the votes on the given division",
    "<c|l> <id>",
    ::fetchVotes,
    false
)

public fun fetchVoteString(house: House, id: String): String {

    val channel = Betty.guild.getTextChannelById(
        when(house){
            House.COMMONS -> "556274285778305024"
            else-> "556274673524932638"
        }
    )

    val message = channel?.retrieveMessageById(id)?.complete()
        ?: throw Exception("Could not locate the message with the id '$id'")

    val votes = TreeMap<Int, Vote>()

    message.reactions
        .map { reaction ->
            val users = reaction.retrieveUsers().complete()
            users.forEach { user ->
                val u = Betty.XOMBP.users.values.firstOrNull { it.id == user.id }
                    ?: Betty.XOMBP.users[user.name.lowercase()]
                    ?: throw Exception("Missing user '${user.name}'")
                val index = Betty.XOMBP.voteRows.getValue(house)[u] ?: -1
                val vote = when(reaction.reactionEmote.name){
                    "abstain" -> when(house){
                        House.COMMONS -> Vote.ABS
                        else -> Vote.PRE
                    }
                    "no" -> when(house){
                        House.COMMONS -> Vote.NO
                        else -> Vote.NOT
                    }
                    "aye", "white_check_mark", "✅" -> when(house){
                        House.COMMONS -> Vote.AYE
                        else -> Vote.CON
                    }
                    else -> Vote.valueOf(reaction.reactionEmote.name.uppercase().take(3))
            }
            user.name to JSONObject()
            votes[index] = vote
        }
     }

    return (0 .. votes.lastKey()).joinToString("\n"){ i ->
        if(votes.containsKey(i))
            votes.getValue(i).toString()
        else
            "N/A"
    }
}

private fun fetchVotes(params: Queue<String>): MessageEmbed {
    val house = when(params.poll()){
        "l" -> House.LORDS
        else -> House.COMMONS
    }
    val id = params.poll()

    val msg = try {
        fetchVoteString(house, id)
    }
    catch(e: Exception){
        return createError(e.message ?: "Unknown exception happened")
    }

    val builder = EmbedBuilder()
    builder.setTitle("Votes")
    builder.setDescription(
        "```\n" + msg +   "```\n"
    )
    return builder.build()
}