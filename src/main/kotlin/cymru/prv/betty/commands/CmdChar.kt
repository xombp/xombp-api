package cymru.prv.betty.commands

import cymru.prv.betty.Betty
import cymru.prv.xombp.Character
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import java.awt.Color
import java.time.LocalDate

/**
 * @author Preben Vangberg
 * @since 1.0.0
 */
object CmdChar: Command(
    "Prints information about the given character",
"[date] <name>",

    /**
     * Fetches information about a specific character.
     *
     * If a date in format YYYY-MM-DD is provided it will print the
     * information as it was on that day. Otherwise it will print it
     * as it is today.
     *
     * It will first try the fullname against all aliases and known names.
     * Then it will try against known surnames
     * Then if everything else fails it will print the 3 closest names.
     */
    { params ->
        val date = fetchDate(params)

        if (params.size == 0)
            createError("Missing parameter. Character name required", "Parameter Error")
        else {
            val name = params.joinToString(separator = " ")
            val lowered = name.lowercase()

            when {
                Betty.XOMBP.characters.containsKey(lowered) -> Betty.XOMBP.characters.getValue(lowered).buildEmbed(date)
                else -> {
                    val set = Betty.XOMBP.characters.entries
                        .filter { it.key.lowercase().contains(lowered) }
                        .map { it.value }
                        .distinct()
                    when (set.size) {
                        0 -> fetchClosestCharacters(date, name, lowered)
                        1 -> set.elementAt(0).buildEmbed(date)
                        else -> fetchCharactersWithSameSurname(date, set)
                    }
                }
            }
        }
    }
)


private fun fetchCharactersWithSameSurname(date: LocalDate, set: List<Character>): MessageEmbed{
    val builder = EmbedBuilder()
    builder.setTitle("Choose Character")
    builder.setColor(Betty.defaultColor)
    builder.setDescription("There are several characters with that match the given query. Please choose one from the list bellow")
    builder.addField(
        "Available characters",
        set.sortedBy { it.name.getValue(date) }.joinToString("\n") {
            "%s %s".format(
                it.affiliation.getValue(date).emote.getValue(date),
                it.getFullName(date)
            )
        },
        false
    )
    return builder.build()
}

private fun fetchClosestCharacters(date: LocalDate, name: String, lowered: String): MessageEmbed {
    val closest = closestMatch(lowered, Betty.XOMBP.characters)

    val builder = EmbedBuilder()
    builder.setColor(Color.red)
    builder.setTitle("Unknown Character")
    builder.setDescription("Unable to find character '$name'. Make sure that you have typed the name correctly")
    builder.addField(
        "Did you mean",
        closest.map { it.name.getValue(date) }.joinToString(separator = "\n"),
        false
    )
    return builder.build()
}