package cymru.prv.betty

import cymru.prv.api.BettyAPI
import cymru.prv.betty.commands.*
import cymru.prv.xombp.FullXOMBP
import cymru.prv.xombp.House
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.JDABuilder
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.MessageEmbed
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import org.apache.commons.text.similarity.LevenshteinDistance
import org.http4k.server.SunHttp
import org.http4k.server.asServer
import org.json.JSONObject
import java.awt.Color
import java.lang.RuntimeException
import java.nio.file.Files
import java.nio.file.Path
import java.util.*
import kotlin.system.exitProcess

/**
 * @author Preben Vangberg
 */
object Betty : ListenerAdapter() {

    val api = BettyAPI()
    var XOMBP = FullXOMBP()
    val guild: Guild by lazy {
        jda.getGuildById(556271746592800772)!!
    }
    val defaultColor = Color.decode("#7a93a9")!!
    private val obj = JSONObject(Files.readString(Path.of("keys.json")))
    private val prefix = obj.optString("prefix", "!betty")
    val jda = JDABuilder.createDefault(obj.getString("discord"))
            .addEventListeners(this)
            .build()
            .awaitReady()

    internal fun start() {
        jda.awaitReady()
        api.server.asServer(SunHttp(port=14000)).start()
        exportToTPS(XOMBP)
        //exitProcess(0)
    }

    override fun onGuildMessageReceived(event: GuildMessageReceivedEvent) {
        if (event.author.isBot)
            return
        if (!event.message.contentDisplay.startsWith("$prefix "))
            return

        val commandParts: Queue<String> = LinkedList(
            event.message.contentDisplay
                .replaceFirst("$prefix ", "")
                .trim()
                .split("\\s+".toRegex())
        )

        val command = commandParts.remove()

        if(command == "vote"){
            val house = when(commandParts.poll()){
                "l" -> House.LORDS
                else -> House.COMMONS
            }
            val id = commandParts.peek()
        }

        if(command == "reload"){
            event.channel.sendMessage(
                if(event.author.id == "184407866906640384") {
                    try {
                        XOMBP = FullXOMBP()
                        val builder = EmbedBuilder()
                        builder.setTitle("Reload successful")
                        builder.setDescription("Data was successfully reloaded from the spreadsheets.")
                        builder.setColor(defaultColor)
                        builder.build()
                    }
                    catch (e: Exception){
                        createError(e.localizedMessage, "Failed to reload the data")
                    }
                }
                else {
                    createError("User not authorised to run that command")
                }
            ).queue()
            return
        }

        event.channel.sendMessage(
            try {
                commands[command]
                    ?.run?.invoke(commandParts)
                    ?: createError(
                        "Unknown command '$command'\nDid you mean ${getClosestCommand(command)}?",
                        title = "Unknown Command"
                    )
            } catch (e: Exception) {
                e.printStackTrace()
                createError(e.message ?: "An unknown error occurred")
            }
        ).queue()
    }
}

private val commands = mapOf(
    "about" to Command(
        "Prints information about the bot",
        "",
        ::about
    ),
    "help" to Command(
        "Prints some (hopefully) helpful information",
        "[command]",
        ::help
    ),
    "list" to Command(
        "Lists all commands",
        "",
        ::list
    ),
    "char" to CmdChar,
    "leg" to CmdLegislation,
    "party" to CmdParty,
    "parties" to CmdParties,
    "position" to CmdPosition,
    "positions" to CmdPositions,
    "user" to CmdUser,
    "cabinet" to CmdCabinet,
    "gov" to CmdGov,
    "votes" to CmdVotes
)

private fun list(params: Queue<String>): MessageEmbed {
    val builder = EmbedBuilder()
    builder.setTitle("List of Commands")
    builder.setColor(Betty.defaultColor)
    val longest = commands.keys.maxOf { it.length }
    builder.setDescription(
        commands.keys.sorted().filter { commands.getValue(it).list }.joinToString(separator = "\n") {
            "`%-${longest}s : %s`".format(
                it,
                commands.getValue(it).desc.split("\n")[0]
            )
        }
    )
    return builder.build()
}

private fun about(params: Queue<String>): MessageEmbed {
    val builder = EmbedBuilder()
    builder.setTitle("About Betty")
    builder.setColor(Betty.defaultColor)
    builder.setThumbnail("https://upload.wikimedia.org/wikipedia/commons/f/f2/Official_portrait_of_Baroness_Boothroyd_crop_2.jpg")
    builder.setDescription(
        "Betty Boothroyd, or Betty for short, is a bot that aggregate data available for"
                + "XO's Model British Parliament in order to make this data more accessible for users and"
                + "other applications. The bot is named after Betty Boothroyd who is a former speaker of the House of Commons"
    )

    builder.addField(
        "Repository",
        "https://gitlab.com/prvInSpace/xombp-api/",
        false
    )
    return builder.build()
}

private fun help(params: Queue<String>): MessageEmbed {
    val builder = EmbedBuilder()
    builder.setColor(Betty.defaultColor)
    if (params.isEmpty()) {
        builder.setTitle("Some general information")
        builder.addField(
            "Information about one command",
            "To see information about a specific command, run `help <command>`",
            false
        )
        builder.addField(
            "List of Commands",
            "To get a list of commands, run `list`",
            false
        )
    } else {
        val commandName = params.remove()
        if (commands.containsKey(commandName)) {
            val command = commands.getValue(commandName)
            builder.setTitle("Command: $commandName")
            builder.setDescription(command.desc)
            if (command.params.isNotBlank()) {
                builder.addField("Parameters", command.params, false)
            }
        } else {
            builder.setTitle("Unknown Command")
            builder.setDescription(
                "Unable to find command `$commandName`.\nDid you mean `${getClosestCommand(commandName)}`?"
            )
        }
    }

    return builder.build()
}

private fun getClosestCommand(commandName: String): String {
    val list = mutableListOf<Pair<Int, String>>()
    for (key in commands.keys)
        list.add(Pair(LevenshteinDistance().apply(commandName, key), key))
    return list.sortedBy { it.first }[0].second
}



fun main() {
    Betty.start()
}