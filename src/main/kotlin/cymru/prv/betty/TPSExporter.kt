package cymru.prv.betty

import cymru.prv.common.DatedVariable
import cymru.prv.xombp.Character
import cymru.prv.xombp.Party
import cymru.prv.xombp.User
import cymru.prv.xombp.XOMBP
import org.json.JSONArray
import org.json.JSONObject
import java.io.File
import java.lang.Exception
import java.time.format.DateTimeFormatter

/**
 * @author Preben Vangberg
 */

fun exportToTPS(xombp: XOMBP){

    File("export").delete()

    xombp.parties.values.distinct().forEach {
        JSONObject()
            .put("id", it.tpsID)
            .putOpt("founded", it.active.getValues().first().from)
            .put("name", it.name.toJson())
            .put("emote", it.emote.toJson())
            .saveToDisk("export/parties/${it.tpsID}.json")
    }

    xombp.characters.values.distinct().forEach {
        val id = it.user.id.ifBlank { 0 }
        JSONObject()
            .put("name", it.name.getValue())
            .put("user", it.user.id)
            .put("id", it.id)
            .put("party", it.affiliation.toJson { party -> party.tpsID })
            .put("img", it.facesteal.link)
            .saveToDisk("export/characters/${id}/${it.id}.json")
    }

    xombp.users.values.distinct().forEach{
        JSONObject()
            .put("id", it.id)
            .put("characters", JSONArray(it.characters.map { it.id }))
            .saveToDisk("export/users/${it.id}.json")
    }

    exportLegislation()

}

private fun exportLegislation(){
    Betty.XOMBP.legislation.forEach {
        it.value.exportToJson().saveToDisk("export/leg/${it.key}.json")
    }
}

private fun JSONObject.saveToDisk(path: String) {
    val file = File(path)
    file.parentFile.mkdirs()
    file.writeText(this.toString(2))
}

private fun <T> DatedVariable<T>.toJson(converter: (T) -> Any = { it.toString() }): JSONObject {
    val obj = JSONObject()
    obj.put("default", converter(this.default))
    this.getValues(includeDefault = false).forEach {
        if(it.from != null)
            obj.put(it.from.atStartOfDay().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME), converter(it.value))
    }
    return obj
}

