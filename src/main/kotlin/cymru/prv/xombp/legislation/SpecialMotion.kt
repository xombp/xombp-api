package cymru.prv.xombp.legislation

import cymru.prv.xombp.House
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import java.awt.Color
import java.lang.Exception
import java.time.LocalDate
import java.util.*

class SpecialMotion(
    id: String,
    name: String,
    private val headers: Map<LocalDate, String>
): Legislation(id, name, LegislationType.OTHER, LegislationStatus.ACTIVE) {

    private val passages: Map<LocalDate, Passage> = headers.keys.associateWith { Passage(this) }

    fun getPassage(date: LocalDate): Passage {
        return passages.getValue(date)
    }

    override fun getPassage(passageNumber: Int): Passage? {
        throw Exception("Special motions should not access passages using numbers")
    }

    override fun getDivisions(): List<Reading> {
        return passages.values
            .flatMap { it.readings }
            .filter { it.votes.isNotEmpty() }
    }

    override fun buildEmbed(date: LocalDate, params: Queue<String>): MessageEmbed {

        val builder = EmbedBuilder()
        builder.setTitle(name)
        builder.setColor(Color.decode(LegislationStatus.PASSED.color))

        if(params.size <= 1) {
            passages.entries.sortedBy { it.key }.forEach {
                builder.addField(
                    headers.getValue(it.key),
                    it.value.readings.joinToString("\n") {
                        "${it.house.emote}`${it.date}`"
                    },
                    false
                )
            }
        }
        else {
            val dateString = params.remove() // The ID itself
            val date: LocalDate = try {
                LocalDate.parse(params.remove())
            }
            catch (e: Exception){
                builder.addField(
                    "Unable to parse date",
                    "unable to parse the given date from parameter '$dateString'. Please ensure that it is written in the format yyyy-mm-dd",
                    false
                )
                return builder.build()
            }

            val house = when {
                params.isEmpty() -> null
                params.remove().uppercase().startsWith("L") -> House.LORDS
                else -> House.COMMONS
            }

            val possibleReadings = passages.values
                .flatMap { it.readings }
                .filter { it.date == date }
                .filter { it.house == house || house == null }

            when(possibleReadings.size){
                0 -> builder.addField(
                    "Could not find reading",
                    "Betty was unable to find the reading based on the data provided. Are you sure you wrote the date and house indicator correctly?",
                    false
                )
                1 -> {
                    val reading = possibleReadings[0]
                    builder.addField("House", reading.house.fullname(), true)
                    builder.addField("Date", reading.date.toString(), true)
                    possibleReadings[0].addVotesToEmbed(builder)
                    builder.setDescription("Division for $name")
                }
                else -> builder.addField(
                    "Too unspecific search",
                    "Found too many readings that matched the given search queries. Please add House indicator as well if you did not include this matches",
                    false
                )
            }
        }
        return builder.build()
    }

}