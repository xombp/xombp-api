package cymru.prv.xombp.legislation

import cymru.prv.common.Exportable
import cymru.prv.xombp.*
import org.json.JSONObject

abstract class Legislation(
    val id: String,
    val name: String,
    val type: LegislationType,
    var status: LegislationStatus
): Embedable, Exportable {

    open fun getTitle(): String {
        return "$id - $name"
    }

    abstract fun getPassage(passageNumber: Int): Passage?
    abstract fun getDivisions(): List<Reading>

    override fun exportToJson(): JSONObject {
        return JSONObject()
            .put("id", id)
            .put("name", name)
            .put("type", type.name)
            .put("status", status.name)
    }
}