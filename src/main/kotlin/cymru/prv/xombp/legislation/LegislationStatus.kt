package cymru.prv.xombp.legislation

/**
 * Enum containing the different statues
 * that a piece of legislation might have
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
enum class LegislationStatus(val header: String, val color: String) {

    ACTIVE("Active", ""),
    AMENDED("Amended", "#ffe599"),
    SPENT("Spent", "#a2c4c9"),
    NOT_YET_IN_FORCE("Not yet in force", "#d9ead3"),
    PARTIALLY_IN_FORCE("Partially in force", "#d9ead3"),
    REPEALED("Repealed", "#ea9999"),
    ROYAL_ASSENT("Royal Assent", "#373250"),
    WITHDRAWN("Withdrawn", "#ffffff"),
    PASSED("Passed", "#373250"),
    REJECTED("Rejected", "#ffffff"),
    UNKNOWN("Unknown", ""),
    SECOND("Second Reading", ""),
    THIRD("Third Reading", ""),
    FIRST("First Reading", ""),
    REPORT("Report Stage", ""),
    SCHEDULED("Scheduled", ""),
    READING("Reading", "");

    companion object {

        /**
         * Converts the given string to a
         * legislation status
         */
        internal fun parse(s: String): LegislationStatus {
            return when (s.uppercase().replace("[CL]: ".toRegex(), "")) {
                "2ND READING" -> SECOND
                "3RD READING" -> THIRD
                "1ST READING" -> FIRST
                "REPORT" -> REPORT
                else -> valueOf(s.uppercase().replace(" ", "_"))
            }
        }
    }

}