package cymru.prv.xombp.legislation

import cymru.prv.common.Exportable
import cymru.prv.xombp.House
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import org.json.JSONObject
import java.awt.Color
import java.lang.Exception
import java.time.LocalDate
import java.util.*

open class Bill(id: String, name: String, status: LegislationStatus): Legislation(id, name, LegislationType.BILL, status) {


    private val passages = mutableMapOf<Int, Passage>()

    override fun getPassage(passageNumber: Int): Passage? {
        return passages[passageNumber]
    }

    override fun getDivisions(): List<Reading> {
        return passages.values
            .flatMap { it.readings }
            .filter { it.votes.isNotEmpty() }
    }

    private val shortName = name.replace("(Act [0-9]+|Bill)".toRegex(), "").trim()

    // Data related to a bill that has
    // received royal assent
    var royalAssent: LocalDate? = null
    var commencement  = ""
    var extent = ""
    val affectedLegislation = mutableSetOf<String>()
    var chapterNumber = ""
    set(value) {
        field = value
            .replace("20[0-9]{2}".toRegex(), "")
            .trim()
    }

    /**
     * Adds a new passage to the map of
     * passages
     */
    fun addPassage(index: Int, passage: Passage){
        passages[index] = passage
    }

    override fun buildEmbed(date: LocalDate, params: Queue<String>): MessageEmbed {
        return when(params.size) {
            0 -> buildInformationAboutBill()
            1 -> buildInformationAboutBill()
            else -> buildInformationAboutDivision(params)
        }
    }

    private fun buildInformationAboutBill(): MessageEmbed {

        val builder = EmbedBuilder()

        builder.setTitle(getTitle())

        val house = when {
            id.startsWith("L") -> House.LORDS
            else -> House.COMMONS
        }
        builder.addField(
            "House",
            house.fullname(),
            true
        )
        builder.addField(
            "Status",
            status.header,
            true
        )

        // If chapter number is not blank that
        // means that the bill is an Act of Parliament
        // hence we can include that information as well.
        if(chapterNumber.isNotBlank()){
            builder.addField(
                "Chapter",
                "${royalAssent!!.year} $chapterNumber",
                true
            )

            builder.addField(
                "Extent",
                extent,
                true
            )
            builder.addField(
                "Royal Assent",
                royalAssent.toString(),
                true
            )
            builder.addField(
                "Commencement",
                commencement,
                true
            )
        }

        if(affectedLegislation.isNotEmpty()){
            builder.addField(
                "Affected Legislation",
                affectedLegislation.sorted().joinToString("\n"){
                    "`$it`"
                },
                false
            )
        }

        builder.setColor(
            when{
                status.color.isNotBlank() -> Color.decode(status.color)
                id.startsWith("L") -> House.LORDS.color
                else -> House.COMMONS.color
            }
        )

        val nextReading = passages
            .map { it.value }
            .flatMap { it.readings }
            .filter { it.date >= LocalDate.now() }
            .minByOrNull { it.date }

        if(nextReading != null && nextReading.date >= LocalDate.now()) {
            builder.addField(
                "Next Reading",
                "%s %s: %s".format(nextReading.house.emote, nextReading.type.header, nextReading.date),
                false
            )
        }

        builder.addField("Link", "https://xombp.neocities.org/legislation/${id.lowercase()}.html",false)

        for(passage in passages){
            builder.addField(when(passage.key){
                1 -> "1st Passage"
                2 -> "2nd Passage"
                3 -> "3rd Passage"
                else -> "${passage.key}th Passage"
            },
                passage.value.readings.sortedBy { it.date }.map {
                    "%s `%-6s %s`".format(
                        it.house.emote,
                        it.type.toString().lowercase().replaceFirstChar { it.uppercase() },
                        it.date
                    )
                }.joinToString(separator = "\n"),
                false
            )
        }
        return builder.build()
        
    }

    private fun buildInformationAboutDivision(params: Queue<String>): MessageEmbed {

        val builder = EmbedBuilder()
        builder.setTitle(getTitle())

        var passageNumber: Int = -1
        var givenHouse: House? = null
        var givenType: ReadingType? = null
        var givenVote: String? = null

        val fullname = params.remove()
        if(fullname.contains(".")){
            val passageString = fullname.split(".")[1]
            passageNumber = passageString.toIntOrNull()
                ?: throw Exception("Could not parse passage number from $fullname")
        }

        params.forEach {
            if(givenHouse == null && it.matches("[CcLl]".toRegex())) {
                givenHouse = when {
                    it.uppercase() == "L" -> House.LORDS
                    else -> House.COMMONS
                }
            }
            else if(givenType == null){
                givenType = when(it.lowercase()) {
                    "1" -> ReadingType.FIRST
                    "1st" -> ReadingType.FIRST
                    "2" -> ReadingType.SECOND
                    "2nd" -> ReadingType.SECOND
                    "r" -> ReadingType.REPORT
                    "3" -> ReadingType.THIRD
                    "3rd" -> ReadingType.THIRD
                    else -> {
                        givenVote = it
                        null
                    }
                }
            }
            else if(givenType != null){
                givenVote = it
            }
            else {
                throw Exception("Unexpected argument '$it'")
            }
        }

        val possibleReadings = passages.filter {
            (passageNumber != -1 && it.key == passageNumber) || (passageNumber == -1)
        }.values.flatMap { it.readings }
            .filter {
                (givenHouse != null && it.house == givenHouse) || givenHouse == null
            }.filter {
                (givenType != null && it.type == givenType) || givenType == null
            }

        if(possibleReadings.size == 1){
            val reading = possibleReadings[0]
            builder.addField("House", reading.house.fullname(), true)
            builder.addField("Reading", reading.type.header, true)
            builder.addField("Date", reading.date.toString(), true)
            builder.setColor(reading.house.color)
            possibleReadings[0].addVotesToEmbed(builder, givenVote)
        }
        else {
            builder.addField(
                "Several matching readings",
                "Several readings matches the search query provided. Try to provide more precise query parameters.",
                false
            )
            builder.addField(
                "Matching readings:",
                possibleReadings.joinToString("\n") { reading ->
                    "`Passage %d `%s` %s %s`".format(
                        passages.entries.find { it.value.readings.contains(reading) }!!.key,
                        reading.house.emote,
                        reading.date,
                        reading.type.header
                    )
                },
                false
            )
        }

        return builder.build()
    }

    override fun exportToJson(): JSONObject {
        val obj = super.exportToJson()
        val stages = passages
            .flatMap { it.value.readings }
            .sortedBy { it.date }
            .mapIndexed { index, reading ->
                index.toString() to reading.exportToJson()
            }.toMap().toMutableMap()

        if(royalAssent != null){
            val id = stages.keys.maxOf { it.toInt() } + 1
            stages[id.toString()] = JSONObject()
                .put("date", royalAssent.toString())
                .put("divisions", JSONObject())
                .put("stage", "ROYAL_ASSENT")
                .put("house", "OTHER")
        }

        obj.put("stages", stages)
        obj.put("longName", name)
        return obj
    }
}