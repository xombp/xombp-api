package cymru.prv.xombp.legislation

import cymru.prv.xombp.*
import java.lang.RuntimeException
import java.net.URL
import java.time.LocalDate

class VoteSpreadsheet(
    private val url: URL,
    private val house: House,
    private val amendmentReading: ReadingType = ReadingType.REPORT,
    private val firstDivisionIndex: Int = 9,
    private val specialDates: Map<Int, LocalDate> = mapOf()
    ) {

    /**
     * Internal class used to represent one single column in the
     * vote spreadsheet. Used to hold both the reading and vote in
     * question
     */
    private data class Column(val reading: Reading, val vote: MutableMap<Character, Vote>)

    /**
     * Function to fetch all of the votes from
     * the spreadsheet and to add it to the XOMBP instance.
     */
    fun fetchVotes(xombp: FullXOMBP, fetchRow: Boolean = false) {
        val lines = url.readText().lines().drop(1)

        // Fetches all the divisions for al
        // of the different columns in the spreadsheet
        val columns = fetchReadingsFromHeaders(xombp, lines[0], lines[1])

        // Used to store the last party just in-case
        // Someone is not a member of a party when they
        // casting votes
        var lastParty = xombp.parties.getValue("speaker")

        // User, Row, Index
        data class RowIndex(val row: Int, val index: Int)
        val rowForUser = mutableMapOf<User, RowIndex>()

        // Loops through every line to gather
        // information about characters, parties, and votes.
        var row = -1;
        lines.drop(2).forEach { line ->
            row += 1
            if(line.isBlank())
                return@forEach

            val values = line.split("\t")

            val charName = Character.fetchCharacterName(values[2])
            val username = when {
                values[3].isBlank() -> charName
                else -> values[3]
            }

            if (username.isBlank() || charName == "Vacant")
                return@forEach

            val user = xombp.users.getOrPut(username.lowercase()) { User(xombp, username) }
            var defaultCharacter = xombp.characters.values.find { it.nameMatches(charName) }
                ?: Character(xombp, charName, user)

            // Sets the last party if
            // the column contains a party name
            lastParty = when {
                values[1].isNotBlank() -> {
                    xombp.parties.getValue(
                        values[1].split("(")[0].trim().lowercase()
                    )
                }
                else -> lastParty
            }

            // Loops through and fetches all of the votes and adds
            // them to the division with the same index
            var lastIndex = -1;
            values.drop(firstDivisionIndex).forEachIndexed { index, voteString ->

                if(voteString == "TBA")
                    return@forEachIndexed

                // Whether the cell contains a vote or not
                if (voteString.matches(Regex("(AYE|NO|ABS|CON|NOT|PRE|DNV)"))) {
                    lastIndex = index
                    // Fetch vote and add it
                    val reading = columns[index] ?: return@forEachIndexed

                    // Fetch active character on that date
                    val character = user.getActiveCharacter(reading.reading.date)
                        ?: defaultCharacter

                    reading.vote[character] = Vote.valueOf(voteString)

                    // Checks if the character's party is empty,
                    // and if it is, it sets the character's party
                    // to the last party it encountered in the spreadsheet.
                    val charParty = character.affiliation.getValue(reading.reading.date)
                    if (charParty == xombp.parties.getValue("empty"))
                        character.affiliation.setValue(reading.reading.date, lastParty)
                }
            }

            if(!rowForUser.containsKey(user) || rowForUser.getValue(user).index < lastIndex)
                rowForUser[user] = RowIndex(row, lastIndex)
        }



        if(fetchRow) {
            xombp.voteRows[house] = rowForUser
                .map { it.key to it.value.row }.toMap()
        }
    }

    /**
     * Creates a list of readings / votes that can
     * be used to determine which vote to add any given
     * vote in any given column to
     */
    private fun fetchReadingsFromHeaders(xombp: FullXOMBP, legislationline: String, readingReadingLine: String): List<Column?>{

        val main = legislationline.split("\t").drop(firstDivisionIndex)
        val second = readingReadingLine.split("\t").drop(firstDivisionIndex)

        var passage = Passage(Motion("Temp", "Temp", LegislationStatus.UNKNOWN))

        return main.mapIndexed { index, readingName ->

            // If both cells are empty, simply return null
            if(readingName.isBlank() && second[index].isBlank())
                return@mapIndexed null

            // If the vote has not happened yet
            if(readingName == "TBA")
                return@mapIndexed null

            // Handle QS and Budget accordingly
            if(readingName == "QS" || readingName == "Budget") {
                val date = specialDates.getValue(index)
                val leg = xombp.legislation.getValue(readingName.uppercase()) as SpecialMotion
                val pass = leg.getPassage(date)
                val reading = Reading(pass, house, ReadingType.READING, date)
                val votes = mutableMapOf<Character, Vote>()
                reading.votes[readingName] = votes
                pass.readings.add(reading)
                return@mapIndexed Column(reading, votes)
            }

            val nameParts = readingName.split(".")
            val name = nameParts[0]

            val passageNumber = when(nameParts.size){
                1 -> 1
                else -> nameParts[1].toInt()
            }

            val type = when(second[index].trim()) {
                "" -> ReadingType.READING
                "2nd" -> ReadingType.SECOND
                "3rd" -> ReadingType.THIRD
                else -> amendmentReading
            }

            // If name is not blank we can fetch the last passage
            // Of the bill with the given name
            if (name.isNotBlank()) {
                passage = xombp.legislation
                    .getValue(name.replace(" R[12]".toRegex(), ""))
                    .getPassage(passageNumber)
                    ?: throw RuntimeException("Missing passage $name.$passageNumber")
            }



            val reading = passage.readings
                .find { it.house == house && it.type == type }
                ?: throw RuntimeException("Missing reading $name.$passageNumber $type ${second[index]}")


            val vote = when {
                second[index].matches(Regex("[A-Za-z].*")) ->
                    "Amendment " +
                            second[index]
                                .replace("Amdt. ", "")
                else -> type.header
            }

            val map = mutableMapOf<Character, Vote>()
            reading.votes[vote] = map
            Column(reading, map)
        }
    }

}