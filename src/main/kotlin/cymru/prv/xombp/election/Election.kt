package cymru.prv.xombp.election

import cymru.prv.commons.json.JsonSerializable
import cymru.prv.xombp.XOMBP
import org.json.JSONObject
import java.time.LocalDate

class Election(xombp: XOMBP, obj: JSONObject): JsonSerializable {

    val name: String = obj.getString("name")
    private val date = LocalDate.parse(obj.getString("date"))
    private val seats = obj.getLong("seats")
    private val registeredVoters = obj.getLong("registeredVoters")
    private val previousElection = obj.optString("previousElection", null)
    private val nextElection = obj.optString("nextElection", null)

    private val nationalResults = obj.getJSONObject("national").toMap().entries.associate { entry ->
        val results =  JSONObject(entry.value as Map<*, *>)
        val candidate = xombp.characters.getValue(results.getString("leader").lowercase())
        xombp.parties.getValue(entry.key) to ElectionResults(candidate, results.getLong("votes"), results.getLong("seats"))
    }

    override fun toJson(): JSONObject {
        val obj = JSONObject()
            .put("name", name)
            .put("date", date)
            .put("seats", seats)
            .put("registeredVoters", registeredVoters)
            .put("previousElection", previousElection)
            .put("nextElection", nextElection)

        if(nationalResults.isNotEmpty()){
            obj.put("national", nationalResults.map { entry ->
                val party = entry.key
                val results = entry.value
                val color = party.color.getValue(date)
                JSONObject()
                    .put("name", party.name.getValue(date))
                    .put("color", String.format("#%02x%02x%02x", color.red, color.green, color.blue))
                    .put("leader", JSONObject()
                        .put("name", results.candidate.name.getValue(date))
                        .put("facesteal", results.candidate.facesteal.link))
                    .put("votes", results.votes)
                    .put("seats", results.seats)
            })
        }

        return obj
    }
}