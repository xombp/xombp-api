package cymru.prv.xombp

enum class PositionType {
    GOVERNMENT, OPPOSITION, OTHER
}