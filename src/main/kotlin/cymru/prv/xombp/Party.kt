package cymru.prv.xombp

import cymru.prv.betty.Betty
import cymru.prv.common.DatedVariable
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import org.json.JSONObject
import java.awt.Color
import java.time.LocalDate
import java.util.*

/**
 * Represents a single political party
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class Party(val XOMBP: XOMBP, val id: String, obj: JSONObject) : Embedable {

    companion object {
        var NEXT_ID = 7L
    }

    // Handle IDs and stuff
    val tpsID = obj.optLong("id", NEXT_ID)
    init {
        if(tpsID == NEXT_ID)
            NEXT_ID++
    }

    val name = DatedVariable<String>(obj, "name")
    val active = DatedVariable(obj.optBoolean("defaultActive", false))

    val color = DatedVariable(obj, "color",
        default = Color.decode("#7a93a9")!!,
        converter = { Color.decode(it as String) }
    )

    var emote = DatedVariable(obj, "emote", default = "")

    val aliases = when {
        obj.has("aliases") -> {
            val aliases = obj.getJSONArray("aliases")
            aliases.map {
                XOMBP.parties[(it as String).lowercase()] = this
                it
            }.toSet()
        }
        else -> mutableSetOf()
    }

    init {
        XOMBP.parties[obj.getString("name").lowercase()] = this

        for(name in name.getValues().map { it.value }.distinct())
            XOMBP.parties[name.lowercase()] = this

        if (obj.has("active")) {
            val activeObj = obj.getJSONObject("active")
            for (date in activeObj.keys())
                active.setValue(LocalDate.parse(date), activeObj.getBoolean(date))
        }
    }

    override fun buildEmbed(date: LocalDate, params: Queue<String>): MessageEmbed {
        val builder = EmbedBuilder()

        builder.setTitle(name.getValue(date))
        builder.setColor(color.getValue(date))

        builder.addField(
            "ID",
            id,
            true
        )

        val changes = active.getValues(includeDefault = false)
        val founded = when {
            changes.isNotEmpty() -> changes[0].from ?: LocalDate.MIN
            else -> LocalDate.MIN
        }

        builder.addField(
            "Founded",
            when(founded){
                LocalDate.MIN -> "Time Immemorial"
                else -> founded.toString()
            },
            true
        )

        builder.addField(
            "Status",
            when {
                active.getValue(date) -> "Active"
                founded > date -> "Not yet founded"
                else -> "Defunct"
            },
            true
        )


        if(emote.getValue(date).isNotBlank()){
            builder.addField(
                "Emote",
                emote.getValue(date),
                true
            )
        }

        val names = name.getValues(date)
        if (names.size > 1) {
            builder.addField(
                "Names",
                names.joinToString(separator = "\n") {
                    val from = (it.from ?: founded).toString()
                    val to = it.to?.toString() ?: "Currently"
                    "`%s - %-10s : %s`".format(
                        from, to, it.value
                    )
                },
                false
            )
        }

        val statusChanges = active.getValues(date, includeDefault = false)
        if(statusChanges.isNotEmpty()){
            builder.addField(
                "Active",
                statusChanges.joinToString(separator = "\n") {
                    val from = (it.from ?: founded).toString()
                    val to = it.to?.toString() ?: "Currently"
                    "`%s - %-10s : %s`".format(
                        from, to, it.value
                    )
                },
                false
            )
        }

        if(aliases.isNotEmpty()){
            builder.addField("Aliases (As found in the spreadsheets)",
                aliases.sorted().joinToString("\n"),
                false)
        }

        val emotes = emote.getValues(date)
        if (emotes.size > 1) {
            builder.addField(
                "Emotes",
                emotes.joinToString(separator = "\n") {
                    val from = (it.from ?: founded).toString()
                    val to = it.to?.toString() ?: "Currently"
                    val emote = it.value
                    "`%s - %-10s : %s`".format(
                        from, to, emote
                    )
                },
                false
            )
        }

        builder.setFooter("Date: $date")
        return builder.build()
    }
}