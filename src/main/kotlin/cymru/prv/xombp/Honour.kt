package cymru.prv.xombp

import cymru.prv.common.DatedVariable
import java.time.LocalDate

/**
 * Represents a single honour that has been given
 * to either a player or character
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class Honour(granted: LocalDate, val prefix: String = "") {
    val active = DatedVariable(false)
    init {
        active.setValue(granted, true)
    }
}