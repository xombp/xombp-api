package cymru.prv.xombp

import cymru.prv.common.DatedVariable
import cymru.prv.xombp.election.Election
import org.json.JSONObject

/**
 * Blank XOMBP instance that is useful
 * for testing.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
open class XOMBP {

    val parties = mutableMapOf<String, Party>()
    init {
        parties["empty"] = Party(
            this, "empty", JSONObject()
                .put("name", "No party affiliation")
                .put("alwaysActive", true)
        )
    }
    val users = mutableMapOf<String, User>()
    val characters = mutableMapOf<String, Character>()
    val positions = mutableMapOf<String, Position>()
    val ministries = DatedVariable<Ministry?>(null)
    val elections = mutableSetOf<Election>()

    val pages by lazy {
        characters.values
            .distinct()
            .flatMap { it.name.getValues() }
            .map { it.value }
            .distinct()
            .associateWith { "character" }
            .plus( elections.map { it.name }.associateWith { "election" })
    }

}