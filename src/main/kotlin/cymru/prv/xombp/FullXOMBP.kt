package cymru.prv.xombp

import cymru.prv.common.GoogleSheet
import cymru.prv.xombp.election.Election
import cymru.prv.xombp.legislation.*
import org.json.JSONObject
import java.io.File
import java.net.URL
import java.nio.file.Files
import java.nio.file.Path
import java.time.LocalDate
import kotlin.Exception

/**
 * A singleton object representing the server as a whole.
 * Contains master lists of everything like legislation, users,
 * and so forth.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class FullXOMBP: XOMBP() {

    private val archiveSheet = "1eYze1MPvVY8AY6Q3Tv-P-Hwm1BFn2Dzunmy5CKqsej8"
    private val officialSheet = "1IQxNJkf9ECDAOIsUUNGnZXAMseLtOsbRzqg7RIHNfcU"

    val legislation = mutableMapOf<String, Legislation>(
        "QS" to SpecialMotion("QS", "Queen's Speech",
            mapOf(
                LocalDate.of(2019, 9, 6) to "Term 2",
                LocalDate.of(2020, 6, 12) to "Term 3",
                LocalDate.of(2021, 1, 8) to "Term 4",
                LocalDate.of(2021, 7, 4) to "Term 5",
                LocalDate.of(2021, 9, 10) to "Term 6"
            )
        ),
        "BUDGET" to SpecialMotion("Budget", "Budget of Her Majesty's Government",
            mapOf(
                LocalDate.of(2019, 10, 20) to "Term 2",
                LocalDate.of(2020, 7, 25) to "Term 3",
                LocalDate.of(2021,6, 6) to "Term 4"
            )
        )
    )

    val voteRows = mutableMapOf<House, Map<User, Int>>()

    init {
        val partyData = JSONObject(Files.readString(Path.of("data", "parties.json")))
        for(id in partyData.keys())
            parties[id] = Party(this, id, partyData.getJSONObject(id))

        val ministryData = JSONObject(Files.readString(Path.of("data", "governments.json")))
        val governments = ministryData.getJSONObject("governments").getJSONObject("ministries")
        for(key in governments.keys()){
            val date = LocalDate.parse(key)
            val ministryObj = governments.getJSONObject(key)
            ministries.setValue(date, Ministry(this, ministryObj, date))
        }

        loadUsers()
        loadCharacters()
        loadHonours()
        loadPeerages()
        loadLegislationData()
        loadPositions()
        loadActsOfParliament()

        loadElections()

        // Verification stage
        verifyPartyMembership()
    }

    private fun loadElections() {
        File("data/elections/").walk().filter { it.isFile }.forEach {
            elections.add(Election(this, JSONObject(Files.readString(it.toPath()))))
        }
    }

    private fun loadLegislationData(){
        // Term 1 Motions
        MotionSpreadsheet(
            GoogleSheet(archiveSheet, "614955929").toURL(),
            intArrayOf(6, 7, 8)
        ).addMotions(this)
        // Term 2 Motions
        MotionSpreadsheet(GoogleSheet(archiveSheet, "1926892805").toURL())
            .addMotions(this)
        // Term 3 Motions
        MotionSpreadsheet(GoogleSheet(archiveSheet, "1966495380").toURL())
            .addMotions(this)
        // Term 4 Motions
        MotionSpreadsheet(GoogleSheet(archiveSheet, "877394146").toURL())
            .addMotions(this)
        // Term 5 Motions
        MotionSpreadsheet(GoogleSheet(archiveSheet, "1060160695").toURL())
            .addMotions(this)
        // Term 6 Motions
        //MotionSpreadsheet(GoogleSheet(officialSheet, "665914011").toURL())
        //    .addMotions(this)

        // Term 1 Bills
        BillSpreadsheet(
            GoogleSheet(archiveSheet, "927477395").toURL(),
            houseIndex = 4,
            headerRows = 3,
            house1Readings = intArrayOf(5, 7, 8, 11),
            house2Readings = intArrayOf(13, 15, 16, 18)
        ).addBills(this)
        // Term 2 (Contains some bugs)
        BillSpreadsheet(
            GoogleSheet(archiveSheet, "595656936").toURL(),
        ).addBills(this)
        // Term 3
        BillSpreadsheet(
            GoogleSheet(archiveSheet, "850814718").toURL(),
        ).addBills(this)
        // Term 4
        BillSpreadsheet(
            GoogleSheet(archiveSheet, "1093252338").toURL(),
        ).addBills(this)
        // Term 5
        BillSpreadsheet(
            GoogleSheet(archiveSheet, "2144698178").toURL(),
        ).addBills(this)
        // Term 6
        //BillSpreadsheet(
        //    GoogleSheet(officialSheet, "2031123097").toURL(),
        //).addBills(this)


        // Commons Term 1
        VoteSpreadsheet(
            GoogleSheet(archiveSheet, "134110620").toURL(),
            House.COMMONS,
            ReadingType.THIRD,
            firstDivisionIndex = 7
        ).fetchVotes(this)
        // Commons Term 2
        VoteSpreadsheet(
            GoogleSheet(archiveSheet, "1046365706").toURL(),
            House.COMMONS,
            specialDates = mapOf(
                2 to LocalDate.of(2019, 9, 6),
                25 to LocalDate.of(2019, 10, 20)
            )
        ).fetchVotes(this)
        // Commons Term 3
        VoteSpreadsheet(
            GoogleSheet(archiveSheet, "480838420").toURL(),
            House.COMMONS,
            specialDates = mapOf(
                0 to LocalDate.of(2020, 6, 12),
                37 to LocalDate.of(2020, 7, 25)
            )
        ).fetchVotes(this)
        // Commons Term 4
        VoteSpreadsheet(
            GoogleSheet(archiveSheet, "231432510").toURL(),
            House.COMMONS,
            specialDates = mapOf(
                0 to LocalDate.of(2021, 1, 8),
                123 to LocalDate.of(2021,6, 6)
            )
        ).fetchVotes(this)
        // Commons Term 5
        VoteSpreadsheet(
            GoogleSheet(archiveSheet, "2016927577").toURL(),
            House.COMMONS,
            specialDates = mapOf(
                0 to LocalDate.of(2021, 7, 4)
            )
        ).fetchVotes(this)
        // Commons Term 6
        //VoteSpreadsheet(
        //    GoogleSheet(officialSheet, "1293177126").toURL(),
        //    House.COMMONS,
        //    specialDates = mapOf(
        //        0 to LocalDate.of(2021, 9, 10)
        //    )
        //).fetchVotes(this, true)

        // Lords Term 1
        VoteSpreadsheet(
            GoogleSheet(archiveSheet, "1401689222").toURL(),
            House.LORDS,
            ReadingType.THIRD,
            firstDivisionIndex = 5
        ).fetchVotes(this)
        // Lords Term 2
        VoteSpreadsheet(
            GoogleSheet(archiveSheet, "393638546").toURL(),
            House.LORDS,
            specialDates = mapOf(
                1 to LocalDate.of(2019, 9, 6)
            )
        ).fetchVotes(this)
        // Lords Term 3
        VoteSpreadsheet(
            GoogleSheet(archiveSheet, "1354927594").toURL(),
            House.LORDS,
            specialDates = mapOf(
                0 to LocalDate.of(2020, 6, 12)
            )
        ).fetchVotes(this)
        // Lords Term 4
        VoteSpreadsheet(
            GoogleSheet(archiveSheet, "1830418206").toURL(),
            House.LORDS,
            specialDates = mapOf(
                0 to LocalDate.of(2021, 1, 8)
            )
        ).fetchVotes(this)
        // Lords Term 5
        VoteSpreadsheet(
            GoogleSheet(archiveSheet, "285657652").toURL(),
            House.LORDS,
            specialDates = mapOf(
                0 to LocalDate.of(2021, 7, 4)
            )
        ).fetchVotes(this)

        // Lords Term 6
        //VoteSpreadsheet(
        //    GoogleSheet(officialSheet, "1461303176").toURL(),
        //    House.LORDS,
        //    specialDates = mapOf(
        //        0 to LocalDate.of(2021, 9, 10)
        //    )
        //).fetchVotes(this, true)
    }

    private fun loadUsers(){

        File("data/users/").walk().filter { it.isFile }.forEach {
            val userObj  = JSONObject(Files.readString(it.toPath()))
            val userName = userObj.getString("name")
            val user = User(this, userName,userObj)
            users[userName.lowercase()] = user
            if(userObj.has("aliases"))
                userObj.getJSONArray("aliases")
                    .forEach { users[(it as String).lowercase()] = user }
        }
    }

    private fun loadCharacters(){
        File("data/characters/").walk().filter { it.isFile }.forEach {
            try {
                val characterObj = JSONObject(Files.readString(it.toPath()))
                val characterName = characterObj.getString("name")
                val username = characterObj.getString("user")
                val user = users.getOrPut(username.lowercase()) { User(this, username) }
                Character(this, characterName, user, characterObj)
            } catch (e: Exception){
                println(it.path)
                throw e
            }
        }
    }

    private fun loadHonours(){

        val lines = GoogleSheet(officialSheet, "1478802031").toURL()
            .readText()
            .lines()
            .drop(2)

        val lowerHonours = mapOf(
            Honours.KOBC to listOf(Honours.OOBC),
            Honours.DOBC to listOf(Honours.OOBC),
            Honours.GBE to listOf(Honours.KBE, Honours.DBE, Honours.CBE, Honours.OBE, Honours.MBE),
            Honours.KBE to listOf(Honours.CBE, Honours.OBE, Honours.MBE),
            Honours.DBE to listOf(Honours.CBE, Honours.OBE, Honours.MBE),
            Honours.CBE to listOf(Honours.OBE, Honours.MBE),
            Honours.OBE to listOf(Honours.MBE),
            Honours.OR to listOf(Honours.MR)
        )

        val userHonours = listOf(
            Honours.GCTL,
            Honours.KOBC,
            Honours.DOBC,
            Honours.OOBC,
            Honours.OR,
            Honours.MR
        )

        var lastDate = LocalDate.ofEpochDay(0)
        var lastUser = users.getOrPut("prv") { User(this, "Prv") }
        lines.forEach {
            val values = it.split("\t")
            val name =
                values[0].split(",")[0]
                    .replace("Sir", "")
                    .trim()

            val user = when {
                values[1].isEmpty() -> lastUser
                values[1] == "N/A" -> return@forEach
                else -> users.getOrPut(values[1].lowercase()) { User(this, values[1]) }
            }
            lastUser = user

            val character = user.characters.find { it.nameMatches(name) } ?: Character(this, name, user)
            if(!user.characters.contains(character))
                user.characters.add(character)

            val honourName = Honours.valueOf(values[9])
            val prefix = values[10]
            val date = when {
                values.size < 12 -> lastDate
                values[11].isEmpty() -> lastDate
                else -> LocalDate.parse(values[11])
            }
            lastDate = date

            val honour = when {
                honourName == Honours.OOBC -> Honour(date)
                prefix == "N/A" -> Honour(date)
                else -> Honour(date, prefix)
            }

            val previousHonours = when{
                userHonours.contains(honourName) -> user.honours
                else -> character.honours
            }

            if(lowerHonours.containsKey(honourName)){
                lowerHonours.getValue(honourName).forEach {
                    if(previousHonours.containsKey(it))
                        previousHonours.getValue(it).active.setValue(date, false)
                }
            }

            if(userHonours.contains(honourName))
                user.honours[honourName] = honour
            else
                character.honours[honourName] = honour
        }
    }

    private fun loadPositions(){
        File("data/positions/").walk().filter { it.isFile }.forEach {
            val obj = JSONObject(Files.readString(it.toPath()))
            try{
                val key = obj.getString("id")
                positions[key] = Position(this, key, obj)
            }
            catch (e: Exception){
                throw Exception("$it: ${e.localizedMessage}")
            }
        }
    }

    private fun loadPeerages(){
        val lines = GoogleSheet(officialSheet, "1705419628")
            .toURL()
            .readText()
            .lines()
            .drop(2)

        lines.forEach {
            val values = it.split("\t")
            if(values[0].startsWith("Any Peers sworn "))
                return@forEach

            val name = Character.fetchCharacterName(values[0])
            val user = users.getOrPut(values[1].lowercase()) { User(this, values[1]) }
            val char = characters.getOrPut(name.lowercase()) { Character(this, name, user) }
            val date = LocalDate.parse(values[6])
            val title = values[2]

            if(!char.peerages.any { it.title == title })
                char.peerages.add(Peerage(date, title))
        }
    }

    private fun loadActsOfParliament() {
        val data = URL("https://docs.google.com/spreadsheets/d/1IQxNJkf9ECDAOIsUUNGnZXAMseLtOsbRzqg7RIHNfcU/export?gid=2051194293&format=tsv").readText()
        data.lines().drop(2).forEach {
            val values = it.split("\t")
            val legislation = legislation[(values[1].split(".")[0])]
                ?: return@forEach

            if(legislation !is Bill)
                throw Exception("Legislation ${values[1]} not classified as bill attempted to convert to Act of Parliament")

            val bill = legislation

            bill.chapterNumber = values[2]
            bill.royalAssent = LocalDate.parse(values[6])
            bill.commencement = when(values[7]){
                "" -> bill.royalAssent.toString()
                else -> values[7]
            }
            bill.status = when(values[8].trim()){
                "Repealed" -> LegislationStatus.REPEALED
                "Amended" -> LegislationStatus.AMENDED
                "Not yet in force" -> LegislationStatus.NOT_YET_IN_FORCE
                "Partially in force" -> LegislationStatus.PARTIALLY_IN_FORCE
                "Spent" -> LegislationStatus.SPENT
                else -> bill.status
            }
            bill.extent = values[9]
            if(values[11].isNotBlank()) {
                values[11].split("\\s*,\\s*".toRegex()).forEach {
                    bill.affectedLegislation.add(it)
                }
            }
        }
    }

    private fun verifyPartyMembership(){
        var count = 0
        characters.values.distinct().forEach { char ->
            char.affiliation.getValues(includeDefault = false).forEach { affil ->
                val from = affil.from!!
                val to = affil.to ?: LocalDate.now()
                val partyStatusDuringAffil = affil.value.active
                    .getValues(after = from, date = to)

                partyStatusDuringAffil
                    .filter { !it.value }
                    .forEach {
                        count++
                        println("${char.name.getValue()}: Part of ${affil.value.name.getValue()} ${from}->${to} but the party was inactive ${it.from}->${it.to}")
                    }
            }
        }
        println("A total of $count party affiliation irregularities")

    }
}