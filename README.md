# XO's Model British Parliament Bot

XO's Model British Parliament (XOMBP) is a roleplay Discord server for UK politics. ([Join here!](https://discord.gg/QJ27ZMKMTP))
While the server has been active a lot of data has been collected and stored about activities on the server.

This bot is trying to parse and make sense of all of this data, inserting missing data when needed so that the data can be used to print information about the server.

## Betty Boothroyd 

The bot is named after the former Speaker of the House of Commons, [Betty Boothroyd](https://en.wikipedia.org/wiki/Betty_Boothroyd).

## Available data

Most of the data in the bot is queryable by date.
This means that you should be able to go back and look at the state of any object at any given date.

### Parties
The bot contains a comprehensive list of all of the parties.
This includes name changes as well as the time the different parties have been active.

### Legislation

The bot contains pretty detailed information about different pieces of legislation.
This includes, but is not limited to: passages with divisions and votes, status changes (where available), authors, and so forth.

### Users

The bot contains information about users such as a list of their characters and personal honours.

### Characters

The API contains information about different characters such as honours, peerages, and so forth. This is also used to determine a characters name at any given point in time.

## Contributing

If you see anything that is wrong or inaccurate, please report it or submit a merge request so that it can be corrected.

## Maintainer

The bot is maintained by Preben "prv" Vangberg &lt;prv@aber.ac.uk&gt;

